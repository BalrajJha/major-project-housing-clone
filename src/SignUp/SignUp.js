import React from "react";
import { Link } from "react-router-dom";
import "./SignUp.css";
import validator from "validator";
import { connect } from "react-redux";
import { addUser } from "../js/actions/index";

function mapDispatchToProps(dispatch) {
    return {
        addUser: user => dispatch(addUser(user))
    };
}

class ConnectedSignUp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            signFirstName: "",
            signLastName: "",
            signEmail: "",
            signPassword: "",
            signRepeatPassword: "",
            signTos: false,
            signError: {
                signFirstNameError: "Please enter your first name",
                signLastNameError: "Please enter your last name",
                signEmailError: "Please enter your email e.g. example@mail.com",
                signPasswordError: "Please enter your password and make sure it's atleast 8 characters",
                signRepeatPasswordError: "Please repeat the above password and make sure it's atleast 8 characters",
                signTosError: "Please make sure you accept our terms and conditions",
            },
            signSubmit: false,
        }

        this.passwordRules = {
            minLength: 8,
            minLowercase: 0,
            minUppercase: 0,
            minNumbers: 0,
            minSymbols: 0,
            returnScore: false,
        }

    }

    handleInputChange = (event) => {
        console.log(event.target, event.target.name, event.target.value);
        const target = event.target;
        const name = target.name;
        const value = target.type === "checkbox" ? target.checked : target.value;

        this.setState({
            [name]: value,
        });
    }

    handleSignUpSubmit = (event) => {
        event.preventDefault();
        const signUpFormErrors = {};

        if (validator.isAlpha(validator.blacklist(this.state.signFirstName, ' '))) {
            signUpFormErrors["signFirstNameError"] = ""
        } else {
            signUpFormErrors["signFirstNameError"] = "Please enter your first name";
        }

        if (validator.isAlpha(validator.blacklist(this.state.signLastName, ' '))) {
            signUpFormErrors["signLastNameError"] = ""
        } else {
            signUpFormErrors["signLastNameError"] = "Please enter your last name";
        }

        if (validator.isEmail(this.state.signEmail)) {
            signUpFormErrors["signEmailError"] = ""
        } else {
            signUpFormErrors["signEmailError"] = "Please enter your email e.g. example@mail.com";
        }

        if (validator.isStrongPassword(this.state.signPassword, this.passwordRules)) {
            signUpFormErrors["signPasswordError"] = ""
        } else {
            signUpFormErrors["signPasswordError"] = "Please enter your password and make sure it's atleast 8 characters";
        }

        if (!validator.isStrongPassword(this.state.signRepeatPassword, this.passwordRules) ||
            this.state.signRepeatPassword !== this.state.signPassword) {
            signUpFormErrors["signRepeatPasswordError"] = "Please repeat the above password and make sure it's atleast 8 characters";
        } else {
            signUpFormErrors["signRepeatPasswordError"] = "";
        }

        if (this.state.signTos) {
            signUpFormErrors["signTosError"] = "";
        } else {
            signUpFormErrors["signTosError"] = "Please make sure you accept our terms and conditions";
        }

        const countOfErrors = Object.values(signUpFormErrors).reduce((sum, error) => {
            sum += error ? 1 : 0;
            console.log(error, sum);
            return sum;
        }, 0);

        if (countOfErrors === 0) {
            console.log("All good");

            this.props.addUser({
                signFirstName: this.state.signFirstName,
                signLastName: this.state.signLastName,
                signEmail: this.state.signEmail,
            });

            console.log(event.target[5].checked);
            event.target[5].checked = false;

            this.setState({
                signFirstName: "",
                signLastName: "",
                signEmail: "",
                signPassword: "",
                signRepeatPassword: "",
                signRepeatPassword: "",
                signTos: false,
                signError: {
                    signFirstNameError: "Please enter your first name",
                    signLastNameError: "Please enter your last name",
                    signEmailError: "Please enter your email e.g. example@mail.com",
                    signPasswordError: "Please enter your password and make sure it's atleast 8 characters",
                    signRepeatPasswordError: "Please repeat the above password and make sure it's atleast 8 characters",
                    signTosError: "Please make sure you accept our terms and conditions",
                },
                signSubmit: true,
            });
        } else {
            this.setState({
                signError: signUpFormErrors,
                signSubmit: false,
            });
        }
    }
    render() {
        return (

            <div className="col-lg-8 form-container pt-5 pb-5 mb-5">

                <form className="col-lg-10 container" onSubmit={this.handleSignUpSubmit}>
                    <h1 className="text-start fs-2">Create new account</h1>
                    <p className="lead text-dark text-start text-muted fs-6 mb-5">Already have an account? <Link to="/signin" >Sign in</Link></p>
                    <div className="form-floating">
                        <input type="text"
                            className="form-control"
                            id="floatingFirstName"
                            placeholder="First name"
                            name="signFirstName"
                            value={this.state.signFirstName}
                            onChange={this.handleInputChange}
                        />
                        <label htmlFor="floatingFirstName" className="text-muted">First name</label>
                        <p className="error mt-0 mb-2">{this.state.signError["signFirstNameError"] ? this.state.signError["signFirstNameError"] : <span>&nbsp;</span>}</p>
                    </div>

                    <div className="form-floating">
                        <input type="text"
                            className="form-control"
                            id="floatingLastName"
                            placeholder="Last name"
                            name="signLastName"
                            value={this.state.signLastName}
                            onChange={this.handleInputChange}
                        />
                        <label htmlFor="floatingLastName" className="text-muted">Last name</label>
                        <p className="error mt-0 mb-2">{this.state.signError["signLastNameError"] ? this.state.signError["signLastNameError"] : <span>&nbsp;</span>}</p>
                    </div>

                    <div className="form-floating">
                        <input type="text"
                            className="form-control"
                            id="floatingEmail"
                            placeholder="name@example.com"
                            name="signEmail"
                            value={this.state.signEmail}
                            onChange={this.handleInputChange}
                        />
                        <label htmlFor="floatingInput" className="text-muted">Email address</label>
                        <p className="error mt-0 mb-2">{this.state.signError["signEmailError"] ? this.state.signError["signEmailError"] : <span>&nbsp;</span>}</p>
                    </div>

                    <div className="form-floating">
                        <input type="password"
                            className="form-control"
                            id="floatingPassword"
                            placeholder="Password"
                            name="signPassword"
                            value={this.state.signPassword}
                            onChange={this.handleInputChange}
                        />
                        <label htmlFor="floatingPassword" className="text-muted">Password</label>
                        <p className="error mt-0 mb-2">{this.state.signError["signPasswordError"] ? this.state.signError["signPasswordError"] : <span>&nbsp;</span>}</p>
                    </div>

                    <div className="form-floating">
                        <input type="password"
                            className="form-control"
                            id="floatingRepeatPassword"
                            placeholder="Repeat Password"
                            name="signRepeatPassword"
                            value={this.state.signRepeatPassword}
                            onChange={this.handleInputChange}
                        />
                        <label htmlFor="floatingRepeatPassword" className="text-muted">Repeat Password</label>
                        <p className="error mt-0 mb-2">{this.state.signError["signRepeatPasswordError"] ? this.state.signError["signRepeatPasswordError"] : <span>&nbsp;</span>}</p>
                    </div>

                    <div className="form-check mb-3">
                        <input className="form-check-input"
                            type="checkbox"
                            id="signup-tos"
                            name="signTos"
                            value={this.state.signTos}
                            onChange={this.handleInputChange}
                        />
                        <label className="form-check-label" htmlFor="signup-tos">
                            You agree to our <a href="https://housing.com/terms-of-use/" target="_blank">terms and conditions</a>
                        </label>

                        <p className="error mt-0 mb-2">{this.state.signError["signTosError"] ? this.state.signError["signTosError"] : <span>&nbsp;</span>}</p>
                    </div>

                    <div className="mb-2">
                        <button type="submit" className="signup-button container px-2 py-3 fs-5">Sign Up</button>
                    </div>

                </form>
            </div>
        );
    }
}

const SignUp = connect(
    null,
    mapDispatchToProps
)(ConnectedSignUp);

export default SignUp;