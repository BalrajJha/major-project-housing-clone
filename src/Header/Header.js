import React from "react";
import "./Header.css"

class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header className="container-fluid text-start text-light">
                <h1><i className="bi bi-chevron-bar-up"></i>Housing.<span className="heading-small">com</span></h1>
            </header>
        );
    }
}

export default Header;