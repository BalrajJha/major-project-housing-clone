import React from "react";
import { Link } from "react-router-dom";
import Header from "../Header/Header";
import "./SignIn.css";
import validator from "validator";

class SignIn extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            logEmail: "",
            logPassword: "",
            logError: {
                logEmailError: "Please enter your email address e.g. example@mail.com",
                logPasswordError: "Please enter your password and make sure it's atleast 8 characters",
            },
            logSubmit: false,
        }
    }

    handleInputChange = (event) => {
        console.log(event.target, event.target.name, event.target.value);
        const target = event.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value,
        });

        this.passwordRules = {
            minLength: 8,
            minLowercase: 0,
            minUppercase: 0,
            minNumbers: 0,
            minSymbols: 0,
            returnScore: false,
        }
    }

    handleLogInSubmit = (event) => {
        event.preventDefault();

        const logInError = {};

        if (validator.isEmail(this.state.logEmail)) {
            logInError["logEmailError"] = ""
        } else {
            logInError["logEmailError"] = "Please enter your email address e.g. example@mail.com";
        }

        if (validator.isStrongPassword(this.state.logPassword, this.passwordRules)) {
            logInError["logPasswordError"] = ""
        } else {
            logInError["logPasswordError"] = "Please enter your password and make sure it's atleast 8 characters";
        }

        const countOfErrors = Object.values(logInError).reduce((sum, error) => {
            sum += error ? 1 : 0;
            console.log(error, sum);
            return sum;
        }, 0);

        if (countOfErrors === 0) {
            console.log("All good");
            this.setState({
                logSubmit: true,
                logError: {
                    logEmailError: "Please enter your email address e.g. example@mail.com",
                    logPasswordError: "Please enter your password and make sure it's atleast 8 characters",
                },
            });
        } else {
            console.log("Validation error");
            this.setState({
                logSubmit: false,
                logError: logInError,
            });
        }
    }

    render() {
        return (
            <>
                <div className="container-fluid signup-header p-2 mb-5">
                    <Header />
                </div>
                <div className="container d-flex justify-content-center">

                    <div className="col-lg-8 form-container pt-5 pb-5 mb-5">

                        <form className="col-lg-10 container" onSubmit={this.handleLogInSubmit}>
                            <h1 className="text-start fs-2">Sign in with your email</h1>
                            <p className="lead text-dark text-start text-muted fs-6 mb-5">Don't have an account yet? <Link to="/signup" >Sign up</Link></p>
                            <div className="form-floating">
                                <input type="text"
                                    className="form-control"
                                    id="floatingEmail"
                                    placeholder="name@example.com"
                                    name="logEmail"
                                    value={this.state.logEmail}
                                    onChange={this.handleInputChange}
                                />
                                <label htmlFor="floatingInput" className="text-muted">Email address</label>
                                <p className="error mt-0 mb-2">{this.state.logError["logEmailError"] ? this.state.logError["logEmailError"] : <span>&nbsp;</span>}</p>
                            </div>

                            <div className="form-floating">
                                <input type="password"
                                    className="form-control"
                                    id="floatingPassword"
                                    placeholder="Password"
                                    name="logPassword"
                                    value={this.state.logPassword}
                                    onChange={this.handleInputChange}
                                />
                                <label htmlFor="floatingPassword" className="text-muted">Password</label>
                                <p className="error mt-0 mb-4">{this.state.logError["logPasswordError"] ? this.state.logError["logPasswordError"] : <span>&nbsp;</span>}</p>
                            </div>

                            <div className="mb-2">
                                <button type="submit" className="signin-button container px-2 py-3 fs-5">Sign In</button>
                            </div>
                        </form>
                    </div>
                </div>
            </>
        );
    }
}

export default SignIn;