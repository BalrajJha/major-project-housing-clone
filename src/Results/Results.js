import React from "react";
import Header from "../Header/Header";
import PropertyListing from "../PropertyListing/PropertyListing";
import "./Results.css";

class Results extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {

        return (
            <>
                <div className="container-fluid results-header p-2 mb-5">
                    <Header />
                </div>
                <div className="property-results">
                    {
                        new Array(10).fill(0).map((element, index) => {
                            return (<PropertyListing key={index} />);
                        })
                    }
                </div>

            </>
        );

    }
}

export default Results;