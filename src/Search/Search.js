import React from "react";
import "./Search.css";
import CreatableSelect from 'react-select/creatable';
import Select from 'react-select';
import SearchTab from "../SearchTab/SearchTab";
import Header from "../Header/Header";

const customCitySelectStyles = {
    control: (styles) => ({
        ...styles,
        borderRadius: "0%",
        minHeight: "3rem",
        padding: "1rem",
    }),
}
const customLocationSelectStyles = {
    control: (styles) => ({
        ...styles,
        borderRadius: "0%",
        padding: "1rem",
        minHeight: "3rem",
    }),
    option: (styles) => ({
        ...styles,
        minHeight: "1rem",
        borderBottom: "1px solid gray"
    })
}

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            activeSearchTab: 0,
            searchTabList: ["Buy", "Rent"],
        }
    }

    colourOptions = [
        { value: 'City', label: 'City', color: '#0052CC', isDisabled: true },
        { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
        { value: 'purple', label: 'Purple', color: '#5243AA' },
        { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
        { value: 'orange', label: 'Orange', color: '#FF8B00' },
        { value: 'yellow', label: 'Yellow', color: '#FFC400' },
        { value: 'green', label: 'Green', color: '#36B37E' },
        { value: 'forest', label: 'Forest', color: '#00875A' },
        { value: 'slate', label: 'Slate', color: '#253858' },
        { value: 'silver', label: 'Silver', color: '#666666' },
    ];

    cityDemo = [
        // { value: 'City', label: 'City', color: '#0052CC', isDisabled: true },
        { value: 'Mumbai', label: 'Mumbai', color: '#00B8D9', isFixed: true },
        { value: 'Bangalore', label: 'Bangalore', color: '#5243AA' },
        { value: 'Delhi', label: 'Delhi', color: '#FF5630', isFixed: true },
        { value: 'Kolkata', label: 'Kolkata', color: '#FF8B00' },
        { value: 'Bhopal', label: 'Bhopal', color: '#FFC400' },
        { value: 'Pune', label: 'Pune', color: '#36B37E' },
        { value: 'Hyderabad', label: 'Hyderabad', color: '#00875A' },
        { value: 'Chennai', label: 'Chennai', color: '#253858' },
    ];
    localityDemo = [
        { value: 'Locality', label: 'Locality', color: '#0052CC', isDisabled: true },
        { value: 'Electronic City', label: 'Electronic City', color: '#00B8D9', isFixed: true },
        { value: 'JP Nagar', label: 'JP Nagar', color: '#5243AA' },
        { value: 'Whitefield', label: 'Whitefield', color: '#FF5630', isFixed: true },
        { value: 'RR Nagar', label: 'RR Nagar', color: '#FF8B00' },
        { value: 'Krishnarajapura', label: 'Krishnarajapura', color: '#FF8B00' },
        { value: 'Uttarahalli Hobli', label: 'RR Uttarahalli Hobli', color: '#FF8B00' },
        { value: 'Devanahalli', label: 'Devanahalli', color: '#FF8B00' },

    ];
    handleCityChange = (newValue, actionMeta) => {
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    }

    handleCityInputChange = (inputValue, actionMeta) => {
        console.group('Input Changed');
        console.log(inputValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    }

    handleLocationChange = (newValue, actionMeta) => {
        console.group('Value Changed');
        console.log(newValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    }

    handleLocationInputChange = (inputValue, actionMeta) => {
        console.group('Input Changed');
        console.log(inputValue);
        console.log(`action: ${actionMeta.action}`);
        console.groupEnd();
    }

    handleSearchTabChange = (event) => {
        console.log(event.target.dataset.index);
        console.log(this.colourOptions);
        this.setState({
            activeSearchTab: Number(event.target.dataset.index),
        });
    }

    handleSelectChange = (event) => {
        console.log(event);
    }
    render() {
        return (
            <div className="container-fluid background pt-2">
                <Header />

                <div className="container col-lg-8 col-md-12 text-center py-5">
                    <div className="row">
                        <h1>Properties to buy in Bengaluru</h1>
                        <p className="lead mb-5">Parr...se Perfect</p>
                    </div>

                    <div className="search-wrapper pt-2 pb-3">
                        {
                            <SearchTab onClick={this.handleSearchTabChange}
                                activeSearchTab={this.state.activeSearchTab}
                                searchTabList={this.state.searchTabList}

                            />
                        }

                        <div className="row gx-0 search">
                            <div className="col-lg-2 city">
                                {/* <CreatableSelect
                                    isClearable
                                    onChange={this.handleCityChange}
                                    onInputChange={this.handleCityInputChange}
                                    options={this.cityDemo}
                                    styles={customCitySelectStyles}
                                    components={{ IndicatorSeparator: () => null }}
                                    noOptionsMessage={() => null}
                                    isValidNewOption={() => false}
                                /> */}
                                <Select
                                    className="basic-single text-start"
                                    classNamePrefix="select"
                                    defaultValue={this.cityDemo[0]}
                                    isClearable={false}
                                    isSearchable={true}
                                    components={{ IndicatorSeparator: () => null }}
                                    name="citySearch"
                                    options={this.cityDemo}
                                    styles={customCitySelectStyles}
                                    onChange={this.handleSelectChange}
                                />
                            </div>
                            <div className="col-lg-8">
                                {/* <CreatableSelect
                                    isClearable
                                    onChange={this.handleLocationInputChange}
                                    onInputChange={this.handleLocalityInputChange}
                                    options={this.localityDemo}
                                    components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                                    placeholder={<div className="text-start">Search for locality, landmark, project or builder</div>}
                                    styles={customLocationSelectStyles}
                                    noOptionsMessage={() => null}
                                    isValidNewOption={() => false}

                                /> */}
                                <Select
                                    className="basic-single text-start"
                                    classNamePrefix="select"
                                    // defaultValue={this.localityDemo[0]}
                                    isClearable={false}
                                    isSearchable={true}
                                    name="citySearch"
                                    options={this.localityDemo}
                                    onChange={this.handleSelectChange}
                                    styles={customLocationSelectStyles}
                                    placeholder={<div className="text-start">Search for locality, landmark, project or builder</div>}
                                    components={{ DropdownIndicator: () => null, IndicatorSeparator: () => null }}
                                />

                            </div>
                            <button className="col-lg-2 search-button" type="button" id="search-button"><i className="bi bi-search m-2"></i>Search</button>
                        </div>
                    </div>
                </div >

            </div>
        );
    }
}

export default Search;