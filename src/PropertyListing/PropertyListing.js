import React from "react";
import "./PropertyListing.css";

class PropertyListing extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (<div className="container property">
            <div className="modal fade" id="contactModal" tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header m-0">
                            <h5 className="fs-6 text-center px-3 py-2 mt-1 modal-results-header"><i className="bi bi-lightning-fill" style={{ color: "yellow" }}></i>Nice choice. Let’s connect with the Developer</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
                        </div>
                        <div className="modal-body">
                            <form>
                                <h6 className="modal-title mb-2" id="exampleModalLabel">Contact Sellers in</h6>
                                <h6 className="fs-6 fw-bold"><img src="https://source.unsplash.com/random/40x40/?branding" className="img-fluid me-3" style={{ borderRadius: "50%" }} alt="..." />Godrej Splendour</h6>

                                <p className="fs-6 text-dark fw-semibold">Please share your contact details</p>
                                <div className="form-floating mb-3">
                                    <input type="text"
                                        className="form-control"
                                        id="recipient-name"
                                        placeholder="Full name"
                                        name="contact-name"
                                    />
                                    <label htmlFor="contact-name" className="text-muted">Full name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input type="tel"
                                        className="form-control"
                                        id="contact-phone"
                                        placeholder="Phone number"
                                    />
                                    <label htmlFor="contact-phone" className="col-form-label text-muted">Phone number</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input type="input"
                                        className="form-control"
                                        id="contact-email"
                                        placeholder="Email"
                                    />
                                    <label htmlFor="contact-email" className="col-form-label text-muted">Email</label>
                                </div>
                                <div className="form-check mb-3">
                                    <input className="form-check-input" type="checkbox" value="" id="contact-marketing" />
                                    <label className="form-check-label" htmlFor="contact-marketing">
                                        I agree to be contacted by Housing and other agents via WhatsApp, SMS, phone, email etc
                                    </label>
                                </div>

                                <div className="form-check mb-2">
                                    <input className="form-check-input" type="checkbox" value="" id="contact-loans" />
                                    <label className="form-check-label" htmlFor="contact-loans">
                                        I am interested in Home Loans
                                    </label>
                                </div>

                            </form>
                        </div>
                        <div className="modal-footer d-flex justify-content-center">
                            <button type="button" className="modal-contact-button container px-2 py-3 fs-5 text-center">Get Contact Details</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="card col-md-10 mb-3 property-card">
                <div className="row g-1">
                    <div className="col-lg-4">
                        <img src="https://source.unsplash.com/random/650x750/?apartment" className="img-fluid" alt="..." />
                    </div>
                    <div className="col-lg-8">
                        <div className="card-body text-dark">
                            <div className="card-title text-dark d-flex justify-content-between flex-wrap">
                                <h5 className="fw-bold fs-4">₹1.03 Cr - 1.16 Cr </h5>
                                <h6 className="social fs-5">EMI starts at ₹51.17 K</h6>
                                <p className="social"><i className="bi bi-share mx-3"></i><i className="bi bi-heart like"></i></p>
                            </div>



                            <div className="text-dark d-flex justify-content-start align-items-baseline">
                                <h5 className="me-2 fs-5">Esteem Splendor&nbsp;</h5>
                                <div className="property-rating">
                                    <h6 className="">4.4 <i className="bi bi-star-fill"></i></h6>
                                </div>
                            </div>
                            <p className="card-text text-dark fs-6 text-muted">By Esteem Group</p>
                            <p className="card-text text-dark fs-6 fw-bold">1, 2 BHK Flats <span className="text-secondary fw-light">for sale in Adugodi</span></p>
                            <p className="card-text text-dark fs-6 mb-4">Koramangala, Bengaluru</p>

                            <div className="card-text property-quick-details d-flex">
                                <div className="property-possession me-5">
                                    <h6 className="text-muted">Possession status</h6>
                                    <h6 className="fw-bold">Ready to move</h6>
                                </div>
                                <div className="property-avg-price">
                                    <h6 className="text-muted">Avg. Price</h6>
                                    <h6 className="fw-bold">₹9.60 K/sq.ft</h6>
                                </div>
                            </div>
                            <p className="card-text text-dark property-description text-muted fs-6"><small>This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.&nbsp;
                                <a className="" data-bs-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    Read more&nbsp;
                                </a>
                                <span className="collapse" id="collapseExample">
                                    <span className="card-text text-dark text-muted">
                                        Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
                                    </span>
                                </span>
                            </small>
                            </p>

                            <div className="property-cta pt-3 mb-1 d-flex flex-row justify-content-between">
                                <div className="col-sm-4 d-flex">
                                    <img src="https://source.unsplash.com/random/40x40/?branding" className="img-fluid me-3" alt="..." />
                                    <div className="property-developer-footer-detail d-flex flex-column">
                                        <h6 className="fw-bold fs-6">
                                            Esteem Group
                                        </h6>
                                        <small>
                                            Developer
                                        </small>
                                    </div>
                                </div>
                                <div className="col-sm-4 property-cta-buttons d-flex justify-content-between">
                                    <button className="cta-phone p-2" type="button">View Phone</button>
                                    <button className="ms-2 cta-contact p-3" type="button" data-bs-toggle="modal" data-bs-target="#contactModal">Contact</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

export default PropertyListing;