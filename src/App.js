import './App.css';
import React from 'react';
import Header from './Header/Header';
import Search from './Search/Search';
import Results from './Results/Results';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import SignIn from './SignIn/SignIn';
import FormSignUp from './FormSignUp/FormSignUp';

class App extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Search />} />
          <Route path="/results" element={<Results />} />
          <Route path="/signup" element={<FormSignUp />} />
          <Route path="/signin" element={<SignIn />} />
        </Routes>
      </BrowserRouter>
    );
  }
}

export default App;
