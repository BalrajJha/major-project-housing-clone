import React from "react";
import Header from "../Header/Header";
import SignUp from "../SignUp/SignUp";
import UserList from "../UserList/UserList";

class FormSignUp extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="container-fluid signup-header p-2 mb-5">
                    <Header />
                </div>

                <div className="container d-flex justify-content-space-between">
                    <SignUp />
                    <UserList />
                </div>
            </div>
        );
    }
}

export default FormSignUp;