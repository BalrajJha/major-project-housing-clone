import React from "react";
import { connect } from "react-redux";
import { v4 as uuidv4 } from 'uuid';

const mapStateToProps = state => {
    return { users: state.users }
}

class ConnectedUserList extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { users } = this.props;
        console.log(users);

        return (<div>
            <h1>Users list</h1>
            <p className="text-dark">Number of users: {users.length}</p>

            {
                users.length === 0 ?
                    <p className="text-dark">No users found</p>
                    :
                    <ul>
                        {users.map((user) => {
                            console.log(user.signFirstName, user.signEmail, user.signLastName);
                            return (<li key={uuidv4()} style={{ color: "black" }}>{user.signFirstName} | {user.signLastName} | {user.signEmail}</li>);
                        })}
                    </ul>
            }

        </div>);
    }
}

const UserList = connect(mapStateToProps)(ConnectedUserList);


export default UserList;