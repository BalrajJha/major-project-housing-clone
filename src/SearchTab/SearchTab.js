import React from "react";
import "./SearchTab.css";

class SearchTab extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row ms-2">
                <div className="d-flex justify-content-start align-items-center py-2">
                    {this.props.searchTabList.map((searchTab, index) => {
                        return (
                            <button key={searchTab}
                                type="button"
                                className={`category-button col-md-1 ${index === this.props.activeSearchTab ? "active-search-tab" : ""}`}
                                onClick={this.props.onClick}
                                value={searchTab}
                                data-index={index}
                            >
                                {searchTab}
                            </button>
                        );
                    })
                    }
                </div>

            </div>
        )
    }
}

export default SearchTab;